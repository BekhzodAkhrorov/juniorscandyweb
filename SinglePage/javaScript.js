let statusProducList = true;
let state = '';
let applyStatus = '';
let length = '';

let product = [
  { model: 'AFF456596', name: 'Flash', price: '150$', capasity: '150' },
  { model: 'AFF456596', name: 'Flash', price: '150$', capasity: '150' },
  { model: 'AFF456596', name: 'Flash', price: '150$', capasity: '150' },
  { model: 'AFF456596', name: 'Flash', price: '150$', capasity: '150' },
  { model: 'AFF456596', name: 'Flash', price: '150$', capasity: '150' },
  { model: 'AFF456596', name: 'Flash', price: '150$', capasity: '150' },
  { model: 'AFF456596', name: 'Flash', price: '150$', capasity: '150' }
];

function productList() {
  if (statusProducList) {
    product.map(
      (item, index) =>
        (document.getElementById('content').innerHTML +=
          '<div class="Info" name="' +
          index +
          '"><div class="checkBox"><input type="checkbox"></div><p>' +
          item.model +
          '</p><p>' +
          item.name +
          '</p><p>' +
          item.price +
          '</p><p>' +
          item.capasity +
          '</p></div><div> </div>')
    );
    statusProducList = false;
  }
  length = product.length;
}

function option(value) {
  if (value == 'add') {
    applyStatus = 'add';
    $(function() {
      $('#content').hide();
      $('#addForm').show();
    });
  }
  if (value == 'delete') {
    applyStatus = 'delete';
    if (length != product.length) {
      document.getElementById('content').innerHTML = '';
      statusProducList = true;
      productList();
    }
    $(function() {
      $('#content').show();
      $('#addForm').hide();
    });
  }
}

function apply() {
  applyStatus == 'add' ? ValidateToArray() : null;

  if (applyStatus == 'delete') {
    $(function() {
      $('#content').toggle(
        function(event) {
          $(this)
            .find('input')
            .attr('checked', true);
        },
        function(event) {
          $(this)
            .find('input')
            .attr('checked', false);
        }
      );
    });

    option(applyStatus);
  }
}

function typeSwetcher(value) {
  let obj = document.getElementById('dynamic');

  value === 'size'
    ? (obj.innerHTML =
        '<select name="select2"><option value ="" disable selected>Choose Option</option><option value="8">8GB</option><option value="16">16GB</option><option value="32">32GB</option><option value="64">64GB</option><option value="128">128GB</option><option value="250">250GB</option></select>')
    : null;

  value === 'weigh'
    ? (obj.innerHTML =
        'Enter how many Kilogram <input type="text" name="weigh" placeholder="Kg"/>')
    : null;

  value === 'dimention'
    ? (obj.innerHTML =
        'Enter Dimention: <input type="text" placeholder="10:10:10" name="dimention"><br />')
    : null;

  state = value;
}

function markUpRedRequred(value) {
  let required = document.getElementById('required');
  value.style.borderColor = 'red';
  required.innerHTML =
    '<p style="background-color:red; color:white; text-align:center">Please Fill the required form *</p>';
  return;
}

function UnMark(value) {
  value.style.border = '1px solid grey';
  return;
}

function ValidateToArray() {
  let addForm = document.getElementById('form');

  if (
    addForm.SKU.value == '' ||
    addForm.name.value == '' ||
    addForm.price.value == '' ||
    addForm.select.value == '' ||
    (addForm.select2 && addForm.select2.value == '') ||
    (addForm.weigh && addForm.weigh.value == '') ||
    (addForm.dimention && addForm.dimention.value == '')
  ) {
    addForm.SKU.value == ''
      ? markUpRedRequred(addForm.SKU)
      : UnMark(addForm.SKU);
    addForm.name.value == ''
      ? markUpRedRequred(addForm.name)
      : UnMark(addForm.name);
    addForm.price.value == ''
      ? markUpRedRequred(addForm.price)
      : UnMark(addForm.price);
    addForm.select.value == ''
      ? markUpRedRequred(addForm.select)
      : UnMark(addForm.select);
    addForm.select2 && addForm.select2.value == ''
      ? markUpRedRequred(addForm.select2)
      : null;
    addForm.weigh && addForm.weigh.value == ''
      ? markUpRedRequred(addForm.weigh)
      : null;
    addForm.dimention && addForm.dimention.value == ''
      ? markUpRedRequred(addForm.dimention)
      : null;
  } else {
    let newItem = {};
    newItem.model = addForm.SKU.value;
    newItem.name = addForm.name.value;
    newItem.price = addForm.price.value;

    switch (state) {
      case 'size':
        newItem.capasity = addForm.select2.value;
        break;
      case 'weigh':
        newItem.capasity = addForm.weigh.value;
        break;
      default:
        newItem.capasity = addForm.dimention.value;
    }
    product.push(newItem);
    applyStatus = 'delete';
    document.querySelector('#default').selected = true;
  }
}
